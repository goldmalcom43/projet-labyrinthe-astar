#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 13/12/2021

@author: Frimpong ADOTRI
"""

# IMPORT DES MODULES
from numpy import sqrt
import heapq as PriotityQueue # FILE DE PRIORITÉ (ANALOGIE JAVA PriorityQueue()) 
from time import time





def creerLabyrinthe(fichier:str) -> list:
    """
    Cette méthode créé et affiche un labyrinthe à partir d'un fichier texte

    Parameters
    ----------
    fichier : str
        DESCRIPTION. chemin d'accès au fichier texte contenant l'architecture du labyrinthe à construire

    Returns
    -------
    list
        DESCRIPTION. La liste contenant l'architecture du labyrinthe à construire

    """
    
    print("\n\nLABYRINTHE INITIAL")
    result = []
    with open(fichier, 'r') as labyrinthe:
        lignes = labyrinthe.read().splitlines()
        for ligne in lignes:
            result.append(ligne)
    for line in result:
        print(line)
    return result





##
# Classe destinée à contenir les outils nécessaires pour déterminer le plus court chemin pour sortir du labyrinthe.
# Il se compose essentiellement d'un constructeur, d'une heurique et d'un algorithme A*
#
##
class  Chemin_labyrinthe(object):
    
    
    def __init__(self, labyrinthe:list, position_debut:str, sortie:str) -> None:
        """
        Cette méthode est le constructeur de notre classe qui recherche le plus court chemin entre la position initiale du damné et la sortie du labyrinthe
        
        Parameters
        ----------
        labyrinthe : list
            DESCRIPTION. Il s'agit d'un labyrinthe codé comme une liste de liste de chaines de caractères
        position_debut : str
            DESCRIPTION. Il indique le caractère marquant la position de début du damné dans le labyrinthe
        sortie : str
            DESCRIPTION. Il indique le caractère marquant la sortie du labyrinthe

        Returns
        -------
        None.

        """
        self.labyrinthe = labyrinthe
        self.coordonnees_position_debut = self.recuperer_coordonnees(position_debut)
        self.coordonnees_sortie = self.recuperer_coordonnees(sortie)
        
     
        
     

     
        
    def recuperer_coordonnees(self, position:str) -> tuple :
        """
        Cette méthode récupère les coordonnées d'un caractère passé en argument

        Parameters
        ----------
        position : str
            DESCRIPTION. caractère modélisant la position dont on souhaite connaitre

        Returns
        -------
        tuple 
            DESCRIPTION. Le tuple correspondant aux coordonnées voulus

        """
        for abscisse, ligne in enumerate(self.labyrinthe):
            for ordonnee, colonne in enumerate(ligne):
                if colonne == position:
                    return (abscisse, ordonnee)
       
        
       
        
       
    
    def recuper_distance(self, chemin:list) -> int:
        """
        Cette méthode retourne la longueur de la liste passée en argument

        Parameters
        ----------
        chemin : list
            DESCRIPTION. Liste contenant l'ensemble des points de passage du chemin solution

        Returns
        -------
        int
            DESCRIPTION. Longueur de la liste contenant les points de passages solutions

        """
        return len(chemin)
    
    
    
    
    def heuristique(self, coordonnees_position:tuple) -> float:
        """
        Cette méthode a pour rôle de calculer l'heuristique d'un point dans le labyrinthe. Ici on utilise plutôt un calcul de distance euclidienne plutôt qu'un calcul de distance de Manhattan

        Parameters
        ----------
        coordonnees_position : tuple
            DESCRIPTION. Coordonnées d'un point dans le labyrinthe

        Returns
        -------
        float
            DESCRIPTION. Heuristique du point correspondant à sa distance euclidienne par rapport au point de sortie

        """
        return sqrt((coordonnees_position[0] - self.coordonnees_sortie[0])**2 + (coordonnees_position[1] - self.coordonnees_sortie[1])**2)
    
    
    
    
  
    
    def prochain_mouvement(self, position_precedente:tuple) -> tuple:
        """
        Cette méthode est une méthode génératrice (ou générateur). Elle génère un itérable suivant un modèle de tuple de directions Haut, Bas, Gauche, Droite

        Parameters
        ----------
        position_precedente : tuple
            DESCRIPTION. Ce tuple correspond aux coordonnées de la position à partir de laquelle, on souhaite rechercher le prochain déplacement approprié

        Yields
        ------
        tuple
            DESCRIPTION.  itérateur correspondant aux configurations en coordonnées des directions Haut, Bas, Gauche, Droite à partir des coordonnées du point passés en argument

        """
        
        mur, mur1, mur2, feu = "+", "-", "|", "F"
        abscisse_precedent, ordonnee_precedent = position_precedente[0], position_precedente[1]
        for prochain_abscisse, prochain_ordonnee in zip((abscisse_precedent+1, abscisse_precedent-1, abscisse_precedent, abscisse_precedent),(ordonnee_precedent, ordonnee_precedent, ordonnee_precedent-1, ordonnee_precedent+1)):
            if (self.labyrinthe[prochain_abscisse][prochain_ordonnee] != mur) and (self.labyrinthe[prochain_abscisse][prochain_ordonnee] != feu) and (self.labyrinthe[prochain_abscisse][prochain_ordonnee] != mur1) and (self.labyrinthe[prochain_abscisse][prochain_ordonnee] != mur2):
                yield (prochain_abscisse, prochain_ordonnee)
                
                
                
                
    def AStar(self)->list:
        """
        Cette méthode est une implémentation de l'algorithme A* avec l'heuristique définie plus haut

        Returns
        -------
        list
            DESCRIPTION. La liste contenant les points de passage du chemin solution. Si cette liste est vide,l'algorithme n'a pas trouvé de chemin de sortie du labyrinthe 

        """
        parcours = [self.coordonnees_position_debut]
        heuristique_initiale = self.recuper_distance(parcours) + self.heuristique(self.coordonnees_position_debut)
        positions_visitees = {self.coordonnees_position_debut : heuristique_initiale}
        file_attente = []
        PriotityQueue.heappush(file_attente, (heuristique_initiale, parcours))

        while len(file_attente) > 0:
            heurisque, parcours = PriotityQueue.heappop(file_attente)
            position_actuelle = parcours[-1]
            
            if position_actuelle == self.coordonnees_sortie:
                return parcours
            for position in self.prochain_mouvement(position_actuelle):
                parcours_tmp = parcours + [position]
                heuristique_position = self.recuper_distance(parcours_tmp) + self.heuristique(position)
                if position in positions_visitees and positions_visitees[position] <= heuristique_position:
                    continue
                positions_visitees[position] = heuristique_position
                PriotityQueue.heappush(file_attente, (heuristique_position, parcours_tmp))
        return []
        
    
    
    
    
    def chemin_solution(self, solution:list) -> list:
        """
        Cette méthode créé une liste contenant la configuration du labyrinthe initial avec les points de passage du chemin solution marqués

        Parameters
        ----------
        solution : list
            DESCRIPTION. La liste contenant les points de passage du chemin solution 

        Returns
        -------
        list
            DESCRIPTION. Liste contenant la configuration du labyrinthe en prenant en soin de marquer les points de passages du chemin solution par un symbole '£'

        """
        labyrinthe_solution = [[point for point in ligne] for ligne in self.labyrinthe]
        
        for point in solution[1:-1]:
            labyrinthe_solution[point[0]][point[1]] = "£"
        labyrinthe_solution[solution[0][0]][solution[0][1]] = "D"
       # if(self.recuperer_coordonnees("S") == solution[-1]):
        labyrinthe_solution[solution[-1][0]][solution[-1][1]] = "S"
        return ["".join(point) for point in labyrinthe_solution]
    
    



def resolution_labyrinthe(fichier:str) -> None:
    """
    Cette méthode simule la recherche du plus court chemin de sortie d'un labyrinthe contenu dans un fichier texte passé en argument

    Parameters
    ----------
    fichier : str
        DESCRIPTION. fichier texte contenant l'architecture d'un labyrinthe

    Returns
    -------
    None
        DESCRIPTION.

    """
    
    labyrinthe = creerLabyrinthe(fichier)
    temps_debut = time()
    lab = Chemin_labyrinthe(labyrinthe, "D", "S")
    chemin = lab.AStar()
    print(chemin)
    temps_fin = time()
    
    if len(chemin) > 0:
        print("\n\nLABYRINTHE AVEC LE CHEMIN SOLUTION")
        print("\n".join(lab.chemin_solution(chemin)))
    else:
        print("\n\nIMPOSSIBLE DE SORTIR DU LABYRINTHE !!!")
    
    print("\n\n---------------------+  Résultats  +----------------------------")
    print("\nTemps d'excécution : {:.2f} seconde(s)".format(temps_fin-temps_debut))
    print(f"Longueur du chemin solution : {lab.recuper_distance(chemin)} points de passage")
    
    
    
    
    
    